var natural = require('natural'),
	classifier = new natural.BayesClassifier(),
	async = require('async'),
	urlParser = require('url'),
	fs = require('fs'),
	_ = require('underscore'),
	lineReader = require('line-reader'),
	trainDir = "./data/train/",
	testDir = "./data/test/",
	cats_trained = [];

function tokenizeURL(url) {
	var result = urlParser.parse(url);
	var r = result.host.split(new RegExp('[:.]'));
	Array.prototype.push.apply(r, result.pathname.split('/'));
	Array.prototype.push.apply(r, result.query.split('&'));
	r.push(result.hash.substr(1));
	r = r.filter(function(e) {
		return e;
	});
	return r;
}

function extractURLText(url) {
	var result = urlParser.parse(url, false, true);
	var strResult = result.host.replace(/\./g, '');
	strResult += result.pathname.replace(/\//g, '').split(".")[0]; // attempt to remove page extension
	if (result.search) strResult += result.query.replace(/\=/g, '').replace(/\&/g, '');
	return strResult;
}

// var testURL = "http://www2.lib.virginia.edu/artsandmedia/artmuseum/africanart/index.html";
// var testURL = "http://www.google.com";

function createnumGrams(url, numGrams) {
	if (!numGrams) numGrams = "all";
	// console.log("Running for " + numGrams + "-grams");
	// Assume it will already be lowercased
	var grams = [],
		i = 0;
	if (numGrams === 4 || numGrams === 5 || numGrams === 6 || numGrams === 7 || numGrams === 8) {
		for (i = 0; i < url.length; i++) {
			grams.push(url.substr(i, numGrams));
			i += numGrams - 1;
		}
	} else if (numGrams === "all") {
		grams = grams.concat(createnumGrams(url, 4));
		grams = grams.concat(createnumGrams(url, 5));
		grams = grams.concat(createnumGrams(url, 6));
		grams = grams.concat(createnumGrams(url, 7));
		grams = grams.concat(createnumGrams(url, 8));
	} else {
		console.log("Not Supported num of grams");
	}
	return grams;
}

classifier.events.on('trainedWithDocument', function(res) {
	// console.log("Training Results");
	// console.log(res);
});





// var testURL = "https://www.google.tt/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=nodejs%20url%20parse";
// var urlTxt = extractURLText(testURL);
// // console.log(urlTxt);
// var allGrams = createnumGrams(urlTxt.toLowerCase());
// // console.log(allGrams);
// classifier.addDocument(allGrams, 'google');
// classifier.train();

// console.log(classifier.getClassifications(allGrams));
// console.log(classifier.classify(allGrams));

// classifier.save('classifier.json', function(err, classifier){
// 	console.log("Classifier Saved");
// });

function processEntity(urlEntity){
	console.log("Processing URL from: " + urlEntity.category);
	var allGrams = createnumGrams(extractURLText(urlEntity.url.toLowerCase()));
	classifier.addDocument(allGrams, urlEntity.category);
}

function onProcessComplete(type){
	console.log("Completed "+ type +" Training");
	classifier.train();
	cats_trained.push(type);
	if (cats_trained.length >= 15){
		console.log("Completed the full Training");
		classifier.save('classifier.json', function(err){
			if (!err)console.log("Classifier persisted");
		});
	}
}


console.log("Running Training");
fs.readdir(trainDir, function(err, files) {
	var data;
	async.each(files, function(file) {
		data = [];
		console.log(file);
		lineReader.eachLine(trainDir + file, function(line, last) {
			data.push(JSON.parse(line.substring(0, line.length - 1)));
		}).then(function(){
			console.log("Completed reading from: " + file);
			console.log("Data array now contains " + data.length + " records");
			async.each(data, processEntity, function(err){
				if (!err)onProcessComplete(data[0].category);
				else console.log(err);
			});
		});

	});
});


// console.log("Running Tests");
// fs.readdir(testDir, function(err, files) {
// 	_.each(files, function(file) {

// 	});
// });