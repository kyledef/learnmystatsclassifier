// https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-14-04
var fs = require('fs'),
	http = require('http'),
	// https = require('https'),
	express = require('express'),
	httpPort= 8080,
	// httpsPort=9090,
//	bodyParser = require("body-parser"),
	natural = require('natural'),
  	classifier = new natural.BayesClassifier(), //https://github.com/NaturalNode/natural
	app = express();
	
// var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8'); //https://www.digitalocean.com/community/tutorials/how-to-create-a-ssl-certificate-on-nginx-for-ubuntu-12-04
// var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
// var credentials = {key: privateKey, cert: certificate};
// var credentials = {};

// natural.BayesClassifier.load("data/links.json", null, function(err, classifier) {
//     console.log(classifier.classify('https://github.com/NaturalNode/natural'));
//     console.log(classifier.classify('https://www.facebook.com/'));
// });

//app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", function(req,res){
	res.send("hello world");
});

app.get("/api/classify", function(req,res){
	console.log(req.data);
	var result = {classify: "unknown"};
	res.json(result);
});

// your express configuration here
http.createServer(app).listen(httpPort, function(){
	console.log("HTTP Server Connected to: " + httpPort);
});
// https.createServer(credentials, app).listen(httpsPort, function(){
// 	console.log("HTTPS Server Connected to: " + httpsPort);
// });