#!/bin/bash

echo "Attempting to generate self signed certificates"
mkdir sslcert
echo "Generating a Certificate Signing Request (CSR)"
openssl genrsa -des3 -out server.key 2048
openssl rsa -in server.key -out server.key.insecure
mv server.key server.key.secure
mv server.key.insecure server.key

echo "Creating the csr"
openssl req -new -key server.key -out server.csr

echo "Creating a Self-Signed Certificate"
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

sudo cp server.crt /etc/ssl/certs
sudo cp server.key /etc/ssl/private

mv server.crt sslcert/
mv server.key sslcert/
mv server.key.secure sslcert/
mv server.csr sslcert/